package main

import (
	"net/http"
)

func searchPage(query string) {}

func main() {
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.ListenAndServe(":3000", nil)
}
